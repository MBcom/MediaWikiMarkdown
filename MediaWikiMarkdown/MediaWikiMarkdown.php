<?php

$wgExtensionCredits['validextensionclass'][] = array(
	'path'           => __FILE__,
	'name'           => 'MediaWikiMarkdown',
	'version'        => '1.0',
	'author'         => 'Maik Boltze', 
	'url'            => 'https://gitlab.informatik.uni-halle.de/ajdcd/MediaWikiMarkdown',
	'description'    => 'Markdown extension für MediaWiki'
);
$wgHooks['ParserFirstCallInit'][] = 'MediaWikiMarkdown::onParserSetup';



include_once("third_party/geshi/geshi.php");
include_once("third_party/markdown/Markdown.php");

class MediaWikiMarkdown{
	/**
     * Register any render callbacks with the parser
	 * @param Parser $parser
	 */
	public static function onParserSetup( $parser ) {
		// When the parser sees the <md> or <markdown> tag, it executes renderTagMarkdown (see below)
		$parser->setHook( 'md', 'MediaWikiMarkdown::renderTagMarkdown' );
        $parser->setHook( 'markdown', 'MediaWikiMarkdown::renderTagMarkdown' );
        return true;
	}

	/**
	 * Returns the converted html (Makrdown -> HTML)
	 * @param mixed $input
	 * @param array $args
	 * @param Parser $parser
	 * @param PPFrame $frame
	 * @return string
	 */
	public static function renderTagMarkdown( $input, $args,  $parser, $frame ) {
		//filter all ```lang
        //              ...
        //           ``` tags
        $code_tags = array();
        preg_match_all('/```[A-Za-z#0-9]*(\r|.|\n)*?```/',$input, $code_tags);
        $code_tags = $code_tags[0];
        foreach($code_tags as $key => $val){
            //replace the code section for the markown parser
            $input = str_replace($val, "{{code:$key}}", $input);

            //get the lang
            $c_lang = array();
            preg_match('/(?<=```)[A-Za-z#0-9]*/', $val, $c_lang);
            $c_lang = $c_lang[0];

            //get the code
            $code = preg_replace(['/\n?```[A-Za-z#0-9]+ *\r?\n?/',
                                 '/\r?\n? *``` *\r?\n?/'], ['',''], $val);


            //the the higlighted value to $code_tags
            $geshi = new GeSHi($code, $c_lang);
            $geshi->enable_line_numbers(GESHI_NORMAL_LINE_NUMBERS);
            $code_tags[$key] = '<code style="border: 1px solid #ddd;border-radius: 5px;display: block;"><div style="border-bottom: 1px solid #ddd;padding: 10px;background-color: #eee;"><strong>'. strtoupper($c_lang).'-Code</strong></div>
<div style="padding: 5px;border: none;">'.$geshi->parse_code().'</div></code>';
        }

        //parse the $input markdown formatted text
        $markdownParser = new Parsedown();
        $input = $markdownParser->text($input);

        //paste the codes back
        foreach ($code_tags as $key => $val){
            $input = preg_replace("/(<p>)?{{code:$key}}(<\/p>)?/", $val, $input);
        }

        //return the ready parsed html
		return $input;
	}
}

?>
