# MediaWikiMarkdown
  
  This MediaWiki extension adds to user defined tags to the MediaWiki:  
  * use `<markdown>...</markdown>` or `<md>...</md>` to write MarkDown code in the MediaWiki  
  * inside the tags you can use  
   ``` ` ` `LANGUAGE   
   ...   
   ` ` `  ```  
 to write highlighted source code like in GitLab  
  
  Add the following line to the end of your 'LocalSettings.php'  
`wfLoadExtension( 'MediaWikiMarkdown' );`  (requires MediaWiki 1.25)  
  (You may need to copy the 'MediaWikiMarkdown' directory into the extensions folder.)  
  
To use this extension with a MediaWiki version below 1.25, include the following to the end of your 'LocalSettings.php':  
`require_once("/extensions/MediaWikiMarkdown/MediaWikiMarkdown/MediaWikiMarkdown.php");`